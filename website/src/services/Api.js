import axios from 'axios'

export default () => {
  return axios.create({
    baseURL: 'https://email.pamelajoa.com/'
    // baseURL: 'http://localhost:3030'
  })
}
