// Initializes the `mail` service on path `/mail`
const createService = require('./mail.class.js');
const hooks = require('./mail.hooks');
const filters = require('./mail.filters');

const nodemailer = require('nodemailer');
const transporter = nodemailer.createTransport({
  host: 'SSL0.OVH.NET',
  port: 465,
  secure: true, // secure:true for port 465, secure:false for port 587
  auth: {
    user: 'contact@pamelajoa.com',
    pass: 'Xh@v2SxfTgn71%'
  }
});

transporter.verify(function(error, success) {
  if (error) {
    console.log(error);
  } else if (success) {
    console.log('Server is ready to take our messages');
  }
});

module.exports = function () {
  const app = this;

  const options = {
    name: 'mail',
    transporter
  };

  // Initialize our service with any options it requires
  app.use('/mail', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('mail');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
