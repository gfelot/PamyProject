const mail = require('./mail/mail.service.js');
module.exports = function () {
  const app = this; // eslint-disable-line no-unused-vars
  app.configure(mail);
};
